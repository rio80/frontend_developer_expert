/* eslint-disable linebreak-style */
/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable linebreak-style */
let checked = false;
const DrawerInitiator = {

  init({ button, drawer, content }) {
    button.addEventListener('click', (event) => {
      this._toggleDrawer(event, drawer);
    });

    content.addEventListener('click', (event) => {
      this._closeDrawer(event, drawer);
    });
  },

  switchChecked() {
    if (checked) {
      checked = false;
    } else {
      checked = true;
    }
    return checked;
  },

  _toggleDrawer(event, drawer) {
    event.stopPropagation();
    const check = this.switchChecked();
    drawer.checked = check;
  },

  _closeDrawer(event, drawer) {
    event.stopPropagation();
    drawer.checked = false;
  },
};

export default DrawerInitiator;
