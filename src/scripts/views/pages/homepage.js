/* eslint-disable linebreak-style */
/* eslint-disable no-plusplus */
/* eslint-disable linebreak-style */
import RestaurantDbSource from '../../data/restaurantdb-source';
import { createMenuItemTemplate } from '../templates/template-creator';

const imageResponsive = ` <picture>
<source media="(max-width: 600px)" srcset="./heros/hero-image_2-small.jpg">
<img class="jumbotron" src='./heros/hero-image_2-large.jpg' alt="Poster Masakan" width="1000" height="300"></img>
</picture>`;

const Homepage = {
  async render() {
    return `
    ${imageResponsive}
    <div class="text-jumbotron" tabindex="0" >Makanan Asli Kranji</div>
    <div class="content" id="maincontent">
      <span class="content-title" tabindex="0">
        Berbagai Makanan Asli dari berbagai suku, mulai dari Jawa, Sunda, Betawi sampai Padang
      </span>
      <p>
        <div class="set-title-content" >
          <div class="title-content" tabindex="0" >
            Pilihan Menu
          </div>
        </div>
        <div class="fa-3x" id="loading">
        </div>
        <div class="card-menu" id="card-menu">
        </div>
      </p>
    </div>
    `;
  },

  async afterRender() {
    // Fungsi ini akan dipanggil setelah render()
    const menus = await RestaurantDbSource.homepageRestaurant();

    const menuContainer = document.querySelector('#card-menu');

    menus.forEach((menu) => {
      menuContainer.innerHTML += createMenuItemTemplate(menu);
    });
  },
};

export default Homepage;
