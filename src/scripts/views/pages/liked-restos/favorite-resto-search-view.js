import { createMenuItemTemplate } from '../../templates/template-creator';

const imageResponsive = ` <picture>
<source media="(max-width: 600px)" srcset="./heros/hero-image_2-small.jpg">
<img class="jumbotron lazyload" data-src='./heros/hero-image_2-large.jpg' alt="Poster Masakan" width="1000" height="300"></img>
</picture>`;

class FavoriteRestoSearchView {
  getTemplate() {
    return `
    ${imageResponsive}
    <div class="text-jumbotron" tabindex="0" >Makanan Asli Kranji</div>
    <div class="content" id="maincontent">
      <span class="content-title" tabindex="0">
        Berbagai Makanan Asli dari berbagai suku, mulai dari Jawa, Sunda, Betawi sampai Padang
      </span>
      <p>
        <div class="set-title-content" >
          <div class="title-content" tabindex="0" >
            Pilihan Menu
          </div>
        </div>
        <input id="query" type="text">
        <div class="card-menu restos" id="card-menu">
            
        </div>
      </p>
    </div>
       `;
  }

  runWhenUserIsSearching(callback) {
    document.getElementById('query').addEventListener('change', (event) => {
      callback(event.target.value);
    });
  }

  showRestos(restos) {
    this.showFavoriteRestos(restos);
  }

  showFavoriteRestos(restos = []) {
    let html;
    if (restos.length) {
      html = restos.reduce((carry, resto) => carry.concat(createMenuItemTemplate(resto)), '');
    } else {
      html = this._getEmptyRestoTemplate();
    }

    document.getElementById('card-menu').innerHTML = html;

    document.getElementById('card-menu').dispatchEvent(new Event('card-menu:updated'));
  }

  _getEmptyRestoTemplate() {
    return '<div class="resto-item__not__found restos__not__found">Tidak ada resto untuk ditampilkan</div>';
  }
}

export default FavoriteRestoSearchView;
