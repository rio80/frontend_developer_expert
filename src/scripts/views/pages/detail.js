/* eslint-disable linebreak-style */
import UrlParser from '../../routes/url-parser';
import RestaurantDbSource from '../../data/restaurantdb-source';
import { createMenuDetailTemplate } from '../templates/template-creator';
import LikeButtonPresenter from '../../utils/like-button-presenter';
import FavoriteRestoIdb from '../../data/favoriteresto-idb';

const Detail = {
  async render() {
    return ` <div class="content" id="maincontentdetail">
    <div class="fa-3x" id="loading">
    </div>
    <div class="detail-menu" id = "detail-menu">
    </div>
    <div id="likeButtonContainer"></div>
    </div>`;
  },

  async afterRender() {
    document.getElementById('button_skip_content').style.display = 'none';
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    const menu = await RestaurantDbSource.detailResto(url.id);
    const detailMenu = document.querySelector('#detail-menu');
    detailMenu.innerHTML = createMenuDetailTemplate(menu.restaurant);

    const resto = menu.restaurant;
    LikeButtonPresenter.init({
      likeButtonContainer: document.querySelector('#likeButtonContainer'),
      favoriteRestos: FavoriteRestoIdb,
      resto,
    });
  },
};

export default Detail;
