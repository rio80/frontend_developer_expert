/* eslint-disable linebreak-style */
import CONFIG from '../../globals/config';

// const setLength = 100;
let setHtml = '';
let picture = '';

const createMenuDetailTemplate = (menu) => {
  picture = CONFIG.BASE_IMAGE_URL_LARGE + menu.pictureId;

  const menusFoods = menu.menus.foods.map((menus) => `<div class="menus-foods-detail">
        ${menus.name}
    </div>`).join(' ');

  const menusDrinks = menu.menus.drinks.map((menus) => `<div class="menus-drinks-detail">
        ${menus.name}
    </div>`).join(' ');

  const menusCategory = menu.categories.map((ct) => `<div class="menus-categories-detail">
        ${ct.name}
        </div>`).join(' ');

  const custReview = menu.customerReviews.map((cr) => `<div class="customer-review-detail-box">
        <div class="header-review-detail">
            <div class="customer-review-name">${cr.name}</div>
            <div class="customer-review-date">${cr.date}</div>
        </div>
        <div class="customer-review-review">${cr.review}</div>
    </div>`).join(' ');

  setHtml = `
    <div class="detail-name" tabindex="0">${menu.name}</div>
    <div class="image-data">
        <img class="detail-image lazyload" data-src="${picture}" alt="${menu.name}" tabindex="0">
        <div class="metadata-menu">
        <table class="table-detail-menu">
            <tr>
                <td class="menu-key" tabindex="0">Nama</td>
                <td> : </td>
                <td tabindex="0">${menu.name}</td>
            </tr>

            <tr>
                <td class="menu-key" tabindex="0">Alamat</td>
                <td>: </td>
                <td tabindex="0">${menu.address}</td>
            </tr>

            <tr>
                <td class="menu-key" tabindex="0">Rating</td>
                <td>: </td>
                <td tabindex="0">${menu.rating}</td>
            </tr>

            <tr>
                <td class="menu-key" tabindex="0">Kategori</td>
                <td>: </td>
                <td tabindex="0">
                <div class="menus-foods-categories">
                ${menusCategory}
                </div>
                </td>
            </tr>
            
        </table>
        </div>
    </div>
    <hr>

    <div class="menus-item">
        <span tabindex="0" class="menu-item-title">
        Menu Makanan :
        </span>
        <div class="menus-foods-item" tabindex="0">
            ${menusFoods}
        </div><br>
        <span tabindex="0" class="menu-item-title">
        Menu Minuman :
        </span>
        <div class="menus-drinks-item" tabindex="0">
            ${menusDrinks}
        </div>
    </div>
    <div class="description-menu">
        <div class="title-desc" tabindex="0">
            Deskripsi : 
        </div>
        <div class="long-desc" tabindex="0">
            ${menu.description}
        </div>
    </div>
    <div class="customer-review" tabindex="0">
    <hr>
        <div class="customer-review-title">Review Customer : </div>
        <div class="detail-review" tabindex="0">
            ${custReview}
        </div>
    </div>
`;
  return setHtml;
};

const createMenuItemTemplate = (menu) => {
  const newMenu = menu;

  picture = CONFIG.BASE_IMAGE_URL_MEDIUM + newMenu.pictureId;

  setHtml = `
    <div class="card-item">
      <img class="card-image lazyload" data-src="${picture}" alt="${newMenu.name || '-'}" srcset="">
      <div class="city">${newMenu.city || '-'}</div>
      <div class="content-card">
        <div class="rating">
          <img class="rating-img lazyload" data-src="./icons/rating.png" alt="" srcset="">
          <div class="text-rating">${newMenu.rating || '-'}</div>
        </div>
        <div class="name">${newMenu.name || '-'}</div>
        <div class="description">${newMenu.description || '-'}</div>
        <a class="resto__title detail-link" tabindex="0" href="${`/#/detail/${newMenu.id}`}">${newMenu.name || '-'}</a>
      </div>
    </div>
  `;

  return setHtml;
};

const createLikeRestoButtonTemplate = () => `
  <button aria-label="like this resto" id="likeButton" class="like">
     <i class="fa fa-heart-o" aria-hidden="true"></i>
  </button>
`;

const createUnlikeRestoButtonTemplate = () => `
  <button aria-label="unlike this resto" id="likeButton" class="like">
    <i class="fa fa-heart" aria-hidden="true"></i>
  </button>
`;

export {
  createMenuDetailTemplate,
  createMenuItemTemplate,
  createLikeRestoButtonTemplate,
  createUnlikeRestoButtonTemplate,
};
