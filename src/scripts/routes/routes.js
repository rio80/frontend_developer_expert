/* eslint-disable linebreak-style */
import Homepage from '../views/pages/homepage';
import Detail from '../views/pages/detail';
import Favorite from '../views/pages/favorite';

const routes = {
  '/': Homepage, // default page
  '/detail/:id': Detail,
  '/favorite': Favorite,
};

export default routes;
