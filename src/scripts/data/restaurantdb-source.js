/* eslint-disable linebreak-style */
import API_ENDPOINT from '../globals/api-endpoint';

class RestaurantDbSource {
  static async homepageRestaurant() {
    const loading = document.querySelector('#loading');
    loading.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
    const response = await fetch(API_ENDPOINT.HOMEPAGE);
    const responseJson = await response.json();
    loading.innerHTML = '';
    return responseJson.restaurants;
  }

  static async detailResto(id) {
    const loading = document.querySelector('#loading');
    loading.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
    const response = await fetch(API_ENDPOINT.DETAIL(id));
    loading.innerHTML = '';
    return response.json();
  }
}

export default RestaurantDbSource;
